library("party")

inputData <- read.csv("http://rstatistics.net/wp-content/uploads/2015/09/ozone1.csv", stringsAsFactors=F)

cf1 <- cforest(ozone_reading ~ . , data= inputData, control=cforest_unbiased(mtry=2,ntree=50))

varimp(cf1) 

library(relaimpo)

lmMod <- lm(ozone_reading ~ . , data = inputData)  # fit lm() model

relImportance <- calc.relimp(lmMod, type = "lmg", rela = TRUE)

sort(relImportance$lmg, decreasing=TRUE)


library(Boruta)

# Decide if a variable is important or not using Boruta
boruta_output <- Boruta(ozone_reading ~ ., data=na.omit(inputData), doTrace=2) 


boruta_signif <- names(boruta_output$finalDecision[boruta_output$finalDecision %in% c("Confirmed", "Tentative")]) 

plot(boruta_output, cex.axis=.7, las=2, xlab="", main="Variable Importance") 