# Data Science Libraries


This repo is supposed to provide the community with data science tutorials, covering from data acquisition and manipulation to machine learning and data visualization.

Here We'll have Python3 and R codes, using several libraries, such as Pandas, ggplot2, dplyr, dfply, NumPy, nnet, ga etc.


